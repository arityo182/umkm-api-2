<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
// use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        // $this->reportable(function (Throwable $e) {
        //     //
        // });
    }

    // public function render($request, Throwable $exception)
    // {
    //     // This will replace our 404 response with
    //     // a JSON response.

    //     if ($this->NotFoundHttpException($exception)) {
    //         switch ($exception->getStatusCode()) {
    //                 // not found
    //             case 404:
    //                 return response()->json(['status' => 'aaa']);
    //                 break;

    //                 // internal error
    //             case '500':
    //                 return response()->json(['status' => 'aaa']);
    //                 break;

    //             default:
    //                 return $this->renderHttpException($exception);
    //                 break;
    //         }
    //     }
    // }
    // else
    // {
    //         return parent::render($request, $e);
    // }
    //     if (
    //         $exception instanceof ModelNotFoundException &&
    //         $request->wantsJson()
    //     ) {
    //         return response()->json([
    //             'data' => 'Resource not found'
    //         ], 404);
    //     }

    //     if ($exception instanceof JWTException) {
    //         return response(['error' => 'Token is not provided'], 500);
    //     }
    //     return parent::render($request, $exception);

    //     return parent::render($request, $exception);


    //     if ($exception instanceof NotFoundHttpException) {
    //         if ($request->is('api/*')) {
    //             return response()->json(['error' => 'Not Found'], 404);
    //         }
    //         return response()->view('404', [], 404);
    //     }
    //     return parent::render($request, $exception);
}
