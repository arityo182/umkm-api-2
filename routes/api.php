<?php

use Illuminate\Support\Facades\Route;

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('register', 'AuthController@register');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('sendverify_mail/{email}', 'AuthController@sendVerifyMail');
    Route::post('verificationMail/{remember_token}', 'AuthController@verificationMail');
    Route::post('verifyOTP', 'AuthController@verifyOTP');
    Route::post('forgot-password', 'AuthController@forgotPassword');
});
// Route::get('/', 'AuthController@index');
